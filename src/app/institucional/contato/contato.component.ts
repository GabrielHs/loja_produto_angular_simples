import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { latLng, MapOptions, tileLayer, Map, Marker, icon } from 'leaflet';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styles: [
  ]
})
export class ContatoComponent implements OnInit {

  constructor() { }

  public map: Map;
  public mapOptions: MapOptions;
  public layers: L.Layer;
  public mcg: any
  public lat = -2.502056;
  public lng = -44.313063;
  public iconCustom: boolean = true;
  

  ngOnInit(): void {
    this.initializeMapOptions()

  }


  onMapReady(map: Map) {
  

    this.map = map;

  }

  public initializeMapOptions() {
    this.mapOptions = {
      center: latLng(-2.5026096, -44.3139957),
      zoom: 12,
      layers: [
        L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 20,
          attribution: '&copy; <a href="//openstreetmap.org/copyright">OpenStreetMap</a> contributors, Points &copy 2012 LINZ'
        })
      ],
    };
    setTimeout(() => {
      this.addLocal()
    }, 1000);
  }


  addLocal() {
    this.mcg = L.markerClusterGroup();

    let tittleMap = `
    <div style="text-align:center">
        <div>
          <b>Loja de produtos</b>
        </div>
        </div>
    </div>
    `

    let myIcon = L.icon({
      iconUrl: `${ this.iconCustom ? 'https://image.flaticon.com/icons/png/512/2113/2113161.png' :'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png'}`,
      iconSize: [40, 41]

    });
    var maker = L.marker(new L.LatLng(this.lat, this.lng), { title: tittleMap, icon: myIcon });




    maker.bindPopup(tittleMap);
    this.mcg.addLayer(maker);
    this.map.addLayer(this.mcg)



  }
}
