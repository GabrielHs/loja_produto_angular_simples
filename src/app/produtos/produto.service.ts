import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Produto } from './produto';
import { Observable } from 'rxjs';

@Injectable()
export class ProdutoService {

    constructor(private http: HttpClient) { }

    obterProdutos(): Observable<Produto[]> {
        return this.http
            .get<Produto[]>(`${environment.urlApi}/produtos`)
    }

    getProduto(id: Number): Observable<Produto> {
        return this.http.get<Produto>(`${environment.urlApi}/produtos/${id}`)
    }
}