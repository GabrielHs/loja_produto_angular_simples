export interface Produto {
    id: number;
    nome: string;
    valor: number;
    promocao: boolean;
    valorPromo: number;
    imagem: string;
}