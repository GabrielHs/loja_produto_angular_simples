import { ProdutoService } from './../produto.service';
import { Component, OnInit } from '@angular/core';
import { Produto } from '../produto';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-produto-detalhe',
  templateUrl: './produto-detalhe.component.html',
  styles: [
  ]
})
export class ProdutoDetalheComponent implements OnInit {

  constructor(private produtoService: ProdutoService, private activatedRoute: ActivatedRoute) { }

  public produto: Produto;
  public produtoid: Number;


  ngOnInit(): void {
    this.getparamentro()

    this.pegarProdutoAtual()
  }

  getparamentro() {
    // 1 forma de pegar paramentro na url
    this.activatedRoute.params.subscribe(params => {
      this.produtoid = params['id'];
    });
  }
  pegarProdutoAtual() {
    // 2 forma de pegar paramentro na url
    // this.produtoid = this.activatedRoute.snapshot.params.id;

    this.produtoService
      .getProduto(this.produtoid).subscribe(
        (data) => {
          this.produto = data;
        },
        (erro) => {
          alert("erro de conexão");
        }
      )

  }

}
