import { ProdutoService } from './../produto.service';
import { Component, OnInit } from '@angular/core';
import { Produto } from '../produto';

@Component({
  selector: 'app-lista-produto',
  templateUrl: './lista-produto.component.html',
  styles: [
  ]
})
export class ListaProdutoComponent implements OnInit {

  constructor(private produtoService: ProdutoService) { }

  public produtos: Produto[];

  ngOnInit(): void {
    this.carregaListaProdutos()
  }

  carregaListaProdutos() {
    this.produtoService.obterProdutos()
      .subscribe(
        (data) => {
          this.produtos = data
        },
        (error) => {
          console.log(error)
        }
      )
  }

}
