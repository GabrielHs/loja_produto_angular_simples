import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styles: [
  ]
})
export class DataBindingComponent implements OnInit {

  public nome: string = "";

  public nome2: string = "";

  public contadorClique: number = 0;

  public urlImagen: string = "https://angular.io/assets/images/logos/angular/angular.svg";

  constructor() { }

  ngOnInit(): void {
  }

  adicionarClique(): void {
    this.contadorClique++;
  }

  zerarContador(): void {
    this.contadorClique = 0;
  }

  keyUp(event: any) {
    this.nome = event.target.value
  }

  pegaTexto() {
    // this.nome2 = this.nome2;
    // ou
    let novoValor = this.nome2
    this.nome2 = novoValor;

  }

}
